import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-mlb-app';
  private apiUrl = 'https://statsapi.mlb.com/api/v1/people?personIds=605151&season=2018&hydrate=stats(type=season,season=2018),education';
  data: any = {};

  constructor(private http: Http) {
    console.log('Hello fellow user');
    this.getPlayer();
    this.getData();
  }

  getData() {
    return this.http.get(this.apiUrl).pipe(map(res => res.json()));
  }

  getPlayer() {
    this.getData().subscribe(data => {
      console.log(data);
      this.data = data;
    });
  }
}
